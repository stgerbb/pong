package ch.ffhs.ftoop.pong;

import javax.swing.JFrame;

/**
 * Diese Klasse repraesentiert ein Fenster
 * 
 * @author Kevin Rossi & Stefan Gerber
 * @since 13/05/2017
 */
public class Fenster extends JFrame {

	// Compiler Warnung beheben
	private static final long serialVersionUID = 1L;

	// Fixe Groesse des Fensters
	private static final int BREITE = 800;
	private static final int HOEHE = 600;

	// Spielfeld-Objekt
	private Spielfeld spielfeld;

	/**
	 * Konstruktor
	 */
	public Fenster() {

		// init
		spielfeld = new Spielfeld();
		this.spielfeld.setSize(getBreite(), getHoehe()); // Spielfeld erhaelt selbe Groesse wie das JFrame

		// JFrame erstellen
		setTitle("Pong");
		setSize(this.getBreite(), this.getHoehe());
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLocationRelativeTo(null);
		setResizable(false);

		add(spielfeld);
		addKeyListener(spielfeld);

		setVisible(true);

		// Starte den Game Loop
		spielfeld.start();	
	}

	// ---------- Getters ---------- //
	/**
	 * Diese Methode gibt die Breite des Fensters zurueck
	 * 
	 * @return Fenster.BREITE - Breite des Fensters
	 */
	public int getBreite() {

		return Fenster.BREITE;
	}
	
	/**
	 * Diese Methode gibt die Hoehe des Fensters zurueck
	 * 
	 * @return Fenster.HOEHE - Hoehe des Fensters
	 */
	public int getHoehe() {

		return Fenster.HOEHE;
	}
}