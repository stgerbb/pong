package ch.ffhs.ftoop.pong;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.Timer;

/**
 * Diese Klasse repraesentiert ein Spielfeld
 * 
 * @author Kevin Rossi & Stefan Gerber
 * @since 13/05/2017
 */
public class Spielfeld extends JPanel implements KeyListener {

	// Compiler Warnung beheben
	private static final long serialVersionUID = 1L;

	// Zwei Spieler
	private Spieler spielerLinks;
	private Spieler spielerRechts;

	// Ball
	private Ball ball;
	
	// Punktestand der Spieler;
	private int punkteSpielerLinks;
	private int punkteSpielerRechts;
	
	// Anzahl Punkte um zu gewinnen
	private static final int MAX_PUNKTE = 5;

	// Timer-Objekt für den Game Loop
	private Timer timer;

	/**
	 * Konstruktor
	 */
	public Spielfeld() {

		// init
		spielerLinks = new Spieler(this, 225, 5, 10, 100);
		spielerRechts = new Spieler(this, 225, 780, 10, 100);

		ball = new Ball(this, 275, 390, 15, 15);
		
		this.punkteSpielerLinks = 0;
		this.punkteSpielerRechts = 0;
	}

	/**
	 * Diese Methode startet den Game Loop
	 */
	public void start() {

		// Game Loop anhand eines Timers
		this.timer = new Timer(6, new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				update();
				render();
			}
		});

		// Starte Game Loop
		this.timer.start();
	}

	/**
	 * Diese Methode beendet den Game Loop
	 */
	public void stop() {

		// Game Loop beenden
		timer.stop();
	}

	/**
	 * Diese Methode updatet die Spiel-Logik
	 */
	public void update() {

		this.spielerLinks.bewegeSpieler();
		this.spielerRechts.bewegeSpieler();
		
		this.ueberpruefePunktestand();
		this.ball.bewegeBall();
	}

	/**
	 * Diese Methode updatet die Grafik
	 */
	public void render() {

		repaint();
	}

	/**
	 * Diese Methode zeichnet das Spielfeld
	 * 
	 * @param g - Um zeichnen zu können
	 */
	@Override
	public void paintComponent(Graphics g) {

		super.paintComponent(g);

		Graphics2D g2d = (Graphics2D) g;
		
		// Schwarzer Hintergrund
		g2d.setColor(Color.BLACK);
		g2d.fillRect(0, 0, getWidth(), getHeight());

		// Mittellinie
		for (int i = 0; i < 30; i++) {
			g2d.setColor(Color.WHITE);
			g2d.fillRect(getWidth() / 2, i * 20, 1, 10);
		}

		// Linker Spieler
		this.spielerLinks.zeichneSpieler(g2d);

		// Rechter Spieler
		this.spielerRechts.zeichneSpieler(g2d);

		// Ball
		this.ball.zeichneBall(g2d);
		
		// Times New Roman als Font mit Grösse 30 für den Punktestand
		g2d.setFont(new Font("TimesRoman", Font.TRUETYPE_FONT, 30));
		
		// Punktestand Spieler Links
		g2d.drawString("" + this.punkteSpielerLinks + "/" + Spielfeld.MAX_PUNKTE, (getWidth() / 2) - 140, getHeight() - 500);
		
		// Punktestand Spieler Rechts
		g2d.drawString("" + this.punkteSpielerRechts + "/" + Spielfeld.MAX_PUNKTE, (getWidth() / 2) + 110, getHeight() - 500);
		
		// Times New Roman als Font mit Grösse 15 für die Autoren
		g2d.setFont(new Font("TimesRoman", Font.TRUETYPE_FONT, 15));
		
		// Autoren
		g2d.drawString("Kevin Rossi & Stefan Gerber", 5, getHeight() - 5);
	}
	
	/**
	 * Diese Methode erhoeht den Punktestand des linken Spielers
	 * 
	 * @param anzahl - Um wieviel der Punktestand erhoeht werden soll
	 */
	public void erhoehePunktestandSpielerLinks(int anzahl) {
		
		this.punkteSpielerLinks += anzahl;
	}
	
	/**
	 * Diese Methode erhoeht den Punktestand des rechten Spielers
	 * 
	 * @param anzahl - Um wieviel der Punktestand erhoeht werden soll
	 */
	public void erhoehePunktestandSpielerRechts(int anzahl) {
		
		this.punkteSpielerRechts += anzahl;
	}
	
	/**
	 * Diese Methode gibt die Punkte des linken Spielers zur�ck.
	 * 
	 * @return - Punkte Spieler links
	 */
	public int getPunkteSpielerLinks() {
		return punkteSpielerLinks;
	}
	
	/**
	 * Diese Methode gibt die Punkte des rechten Spielers zur�ck.
	 * 
	 * @return - Punkte Spieler rechts
	 */
	public int getPunkteSpielerRechts() {
		return punkteSpielerRechts;
	}

	/**
	 * Diese Methode ueberprueft, wer der Sieger ist.
	 */
	public void ueberpruefePunktestand() {
		
		// Der linke Spieler gewinnt
		if(this.punkteSpielerLinks == Spielfeld.MAX_PUNKTE) {
			
			JOptionPane.showMessageDialog(null, "Der linke Spieler hat gewonnen!");
			
			this.stop();
			System.exit(0);
			return;
			
			// Der rechte Spieler gewinnt
		} else if (this.punkteSpielerRechts == Spielfeld.MAX_PUNKTE) {
			
			JOptionPane.showMessageDialog(null, "Der rechte Spieler hat gewonnen!");
			
			this.stop();
			System.exit(0);
			return;
		}
	}
	
	// ---------- KeyListener ---------- //
	/**
	 * Diese Methode ueberprueft, welche Taste gedrueckt wurde
	 * 
	 * @param e - Um Tasten erkennen zu koennen
	 */
	@Override
	public void keyPressed(KeyEvent e) {

		// Spieler Rechts bewegt sich nach oben
		if (e.getKeyCode() == KeyEvent.VK_UP) {
			
			spielerRechts.setVelY(-3);
			return;
				
			// Spieler Rechts bewegt sich nach unten
		} else if (e.getKeyCode() == KeyEvent.VK_DOWN) {
				
			spielerRechts.setVelY(3);
			return;
			
			// Spieler Links bewegt sich nach oben
		} else if (e.getKeyCode() == KeyEvent.VK_W) {
			
			spielerLinks.setVelY(-3);
			return;
			
			// Spieler Links bewegt sich nach unten
		} else if (e.getKeyCode() == KeyEvent.VK_S) {
			
			spielerLinks.setVelY(3);
			return;
		}
	}
	
	/**
	 * Diese Methode ueberprueft, welche Taste losgelassen wurde
	 * 
	 * @param e - Um Tasten erkennen zu koennen
	 */
	@Override
	public void keyReleased(KeyEvent e) {
		
		// Rechter Spieler hoert auf sich zu bewegen
		if (e.getKeyCode() == KeyEvent.VK_UP) {
				
			spielerRechts.setVelY(0);
			return;
		
			// Rechter Spieler hoert auf sich zu bewegen
		} else if (e.getKeyCode() == KeyEvent.VK_DOWN) {
			
			spielerRechts.setVelY(0);
			return;
			
			// Linker Spieler hoert auf sich zu bewegen
		} else if (e.getKeyCode() == KeyEvent.VK_W) {
			
			spielerLinks.setVelY(0);
			return;
			
			// Linker Spieler hoert auf sich zu bewegen
		} else if (e.getKeyCode() == KeyEvent.VK_S) {
			
			spielerLinks.setVelY(0);
			return;
		}
	}

	@Override
	public void keyTyped(KeyEvent e) {

	}

	// ---------- Getters ---------- //
	/**
	 * Diese Klasse gibt ein Spieler-Objekt zurueck
	 * 
	 * @param spieler - Name des Spielers
	 * @return this.spielerLinks oder this.spielerRechts - Spieler-Objekt wird zurueckgegeben
	 */
	public Spieler getSpielerObj(String spieler) {
		if (spieler.equals("spielerLinks")) {
			
			return this.spielerLinks;
			
		} else {
			
			return this.spielerRechts;
		}
	}
}