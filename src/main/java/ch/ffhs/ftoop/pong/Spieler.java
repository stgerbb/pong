package ch.ffhs.ftoop.pong;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;

/**
 * Diese Klasse repraesentiert ein Spieler
 * 
 * @author Kevin Rossi & Stefan Gerber
 * @since 13/05/2017
 */
public class Spieler {

	// Groesse des Spielers
	private int breiteSpieler;
	private int hoeheSpieler;

	// Position des Spielers auf dem Spielfeld
	private int xKordinate;
	private int yKordinate;
	
	// Geschwindigkeit der Bewegung des Spielers entlang der y-Achse
	private int velY;

	// Spielfeld-Objekt
	private Spielfeld spielfeld;

	/**
	 * Konstruktor
	 * 
	 * @param spielfeld - Auf welchem sich der Spieler befinden wird
	 * @param yKordinate - Position anhand der y-Achse
	 * @param xKordinate - Position anhand der x-Achse
	 * @param breiteSpieler - Breite des Spielers
	 * @param hoeheSpieler - Hoehe des Spielers
	 * @param bewegungHoch - Taste um den Spieler nach oben zu bewegen
	 * @param bewegungRunter - Taste um den Spieler nach unten zu bewegen
	 */
	public Spieler(Spielfeld spielfeld, int yKordinate, int xKordinate, int breiteSpieler, int hoeheSpieler) {

		// init
		this.spielfeld = spielfeld;
		this.yKordinate = yKordinate;
		this.xKordinate = xKordinate;
		this.breiteSpieler = breiteSpieler;
		this.hoeheSpieler = hoeheSpieler;
		
		this.velY = 0;
	}

	/**
	 * Diese Methode zeichnet einen Spieler
	 * 
	 * @param g - Um zeichnen zu können
	 */
	public void zeichneSpieler(Graphics g) {

		Graphics2D g2d = (Graphics2D) g;

		// Spieler wird anhand eines Rechtecks gezeichnet
		g2d.fillRect(this.xKordinate, this.yKordinate, this.breiteSpieler, this.hoeheSpieler);
	}
	
	/**
	 * Diese Methode updatet die Bewegung des Spielers
	 */
	public void bewegeSpieler() {
			
		// Setzt den Spieler in bewegung
		this.yKordinate += this.velY;
		
		// Spieler kann nicht durch oberen Rand
		if (this.yKordinate <= 0) {
			
			this.yKordinate = 0;
			return;
			
			// Spieler kann nicht durch den unteren Rand
		} else if (this.yKordinate >= (spielfeld.getHeight() - this.hoeheSpieler)) {
			
			this.yKordinate = (spielfeld.getHeight() - this.hoeheSpieler);
			return;
		}
	}

	// ---------- Getters ---------- //
	/**
	 * Diese Methode gibt ein Rectangle zurueck, welches den Spieler raepresentiert
	 * 
	 * @return new Rectangle - Raepresentiert den Spieler
	 */
	public Rectangle getSpieler() {
		
		return new Rectangle(this.xKordinate, this.yKordinate, this.breiteSpieler, this.hoeheSpieler);
	}
	
	/**
	 * Diese Methode setzt die Bewegungsgeschwindigkeit des Spielers entland der y-Achse
	 * 
	 * @param anzahl - Geschwindigkeit angeben
	 */
	public void setVelY(int anzahl) {
		
		this.velY = anzahl;
	}
	
	/**
	 * Diese Methode gibt die Geschwindigkeit des Spielers zurueck
	 * 
	 * @return this.velY - Geschwindigkeit des Spielers
	 */
	public int getVelY() {
		
		return this.velY;
	}
	
	/**
	 * Diese Methode gibt die Breite des Spielers zurueck
	 * 
	 * @return this.breiteSpieler - Breite des Spielers
	 */
	public int getBreiteSpieler() {
		
		return this.breiteSpieler;
	}
	
	/**
	 * Diese Methode gibt die Hoehe des Spielers zurueck
	 * 
	 * @return this.hoeheSpieler - Hoehe des Spielers
	 */
	public int getHoeheSpieler() {
		
		return this.hoeheSpieler;
	}
	
	/**
	 * Diese Methode gibt die xKoordinate des Spielers zurueck
	 * 
	 * @return this.xKordinate - x-Koordinate des Spielers
	 */
	public int getxKordinate() {
		
		return this.xKordinate;
	}
	
	/**
	 * Diese Methode gibt die yKoordinate des Spielers zurueck
	 * 
	 * @return this.yKordinate - y-Koordinate des Spielers
	 */
	public int getyKordinate() {
		
		return this.yKordinate;
	}
}