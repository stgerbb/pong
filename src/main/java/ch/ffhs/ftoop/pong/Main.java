package ch.ffhs.ftoop.pong;

/**
 * Diese Klasse startet die Applikation
 * 
 * @author Kevin Rossi & Stefan Gerber
 * @since 13/05/2017
 */
public class Main {

	/**
	 * Diese Methode startet die Applikation
	 * 
	 * @param args - Argumente, welche mitgegeben werden koennen
	 */
	public static void main(String[] args) {

		new Fenster();
	}
}