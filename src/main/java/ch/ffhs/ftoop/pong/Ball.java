package ch.ffhs.ftoop.pong;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;

/**
 * Diese Klasse repraesentiert ein Ball
 * 
 * @author Kevin Rossi & Stefan Gerber
 * @since 13/05/2017
 */
public class Ball {

	// Groesse des Balls
	private int breiteBall;
	private int hoeheBall;

	// Position des Balls
	private int yKordinate;
	private int xKordinate;

	// Spielfeld-Objekt
	private Spielfeld spielfeld;

	// Geschwindigkeit des Balls
	private int ballSpeed;

	// Richtung des Balls anhand der x- und y-Achse
	private int xBallRichtung;
	private int yBallRichtung;

	/**
	 * Konstruktor
	 * 
	 * @param spielfeld - Auf welchem sich der Ball befinden wird
	 * @param yKordinate - Position anhand der y-Achse
	 * @param xKordinate - Position anhand der x-Achse
	 * @param breiteBall - Breite des Balls
	 * @param hoeheBall - Hoehe des Balls
	 */
	public Ball(Spielfeld spielfeld, int yKordinate, int xKordinate, int breiteBall, int hoeheBall) {

		// init
		this.spielfeld = spielfeld;
		this.yKordinate = yKordinate;
		this.xKordinate = xKordinate;
		this.breiteBall = breiteBall;
		this.hoeheBall = hoeheBall;
		
		this.ballSpeed = 3;
		this.xBallRichtung = this.ballSpeed;
		this.yBallRichtung = this.ballSpeed;
	}

	/**
	 * Diese Methode zeichnet einen Ball
	 * 
	 * @param g - Um zeichnen zu können
	 */
	public void zeichneBall(Graphics g) {

		Graphics2D g2d = (Graphics2D) g;

		// Ball wird anhand einer Ovale gezeichnet
		g2d.fillOval(this.xKordinate, this.yKordinate, this.breiteBall, this.hoeheBall);
	}

	/**
	 * Diese Methode setzt den Ball in bewegung
	 */
	public void bewegeBall() {

		// Bewege den Ball horizontal nach Links bis zum Rand
		if (this.xKordinate + this.xBallRichtung <= 0) {

			// Spieler Rechts erziehlt einen Punkt
			spielfeld.erhoehePunktestandSpielerRechts(1);
			this.resetPositionBall();
			return;

			// Bewege den Ball horizontal nach Rechts bis zum Rand
		}	else if (this.xKordinate + this.xBallRichtung >= (spielfeld.getWidth() - this.breiteBall)) {

			// Spieler Links erziehlt einen Punkt
			this.spielfeld.erhoehePunktestandSpielerLinks(1);
			this.resetPositionBall();
			return;
			
			// Bewege den Ball vertikal nach oben bis zum Rand
		} else if (this.yKordinate + this.yBallRichtung <= 0) {

			this.yBallRichtung = this.ballSpeed;
			return;

			// Bewege den Ball vertikal nach unten bis zum Rand
		} else if (this.yKordinate + this.yBallRichtung >= (spielfeld.getHeight() - this.breiteBall)) {

			this.yBallRichtung = -this.ballSpeed;
			return;
		}

		this.kollisionMitSpieler();

		// Setzt den Ball in bewegung
		this.xKordinate += this.xBallRichtung;
		this.yKordinate += this.yBallRichtung;
	}

	/**
	 * Diese Methode ueberprueft, ob der Ball mit einem Spieler kollidiert
	 */
	public void kollisionMitSpieler() {

		// Ball kollidiert mit dem linken Spieler
		if (spielfeld.getSpielerObj("spielerLinks").getSpieler().intersects(getBall())) {

			this.xBallRichtung = -this.xBallRichtung;
			return;

			// Ball kollidiert mit dem rechten Spieler
		} else if (spielfeld.getSpielerObj("spielerRechts").getSpieler().intersects(getBall())) {

			this.xBallRichtung = -this.xBallRichtung;
			return;
		}
	}

	/**
	 * Diese Methode setzt dem Ball Default-Werte
	 */
	public void resetPositionBall() {

		// Der Ball wird auf seine Default-Werte resetet
		this.xKordinate = 390;
		this.yKordinate = 275;
		this.ballSpeed = 3;
	}

	// ---------- Getters ---------- //
	/**
	 * Diese Methode gibt ein Rectangle zurueck, welches den Ball raepresentiert
	 * 
	 * @return new Rectangle - Raepresentiert den Ball
	 */
	public Rectangle getBall() {

		return new Rectangle(this.xKordinate, this.yKordinate, this.breiteBall, this.hoeheBall);
	}
	
	/**
	 * Diese Methode gibt die Breite des Balls zurueck
	 * 
	 * @return this.breiteBall - Breite des Balls
	 */
	public int getBreiteBall() {
		
		return this.breiteBall;
	}
	
	/**
	 * Diese Methode gibt die Hoehe des Balls zurueck
	 * 
	 * @return this.hoeheBall - Hoehe des Balls
	 */
	public int getHoeheBall() {
		
		return this.hoeheBall;
	}
	
	/**
	 * Diese Methode gibt die yKoordinate des Balls zurueck
	 * 
	 * @return  this.yKordinate - y-Koordinate des Balls
	 */
	public int getyKordinate() {
		
		return this.yKordinate;
	}
	
	/**
	 * Diese Methode gibt die xKoordinate des Balls zurueck
	 * 
	 * @return this.xKordinate - x-Koordinate des Balls
	 */
	public int getxKordinate() {
		
		return this.xKordinate;
	}
	
	/**
	 * Diese Methode gibt den Speed des Balls zurueck
	 * 
	 * @return this.ballSpeed - Speed des Balls
	 */
	public int getBallSpeed() {
		
		return this.ballSpeed;
	}
	
	/**
	 * Diese Methode gibt die Bewegeungsrichtung in der xAchse des Balls zurueck
	 * 
	 * @return this.xBallRichtung - Bewegeungsrichtung in der x-Achse des Balls
	 */
	public int getxBallRichtung() {
		
		return this.xBallRichtung;
	}
	
	/**
	 * Diese Methode gibt die Bewegeungsrichtung in der yAchse des Balls zurueck
	 * 
	 * @return this.yBallRichtung - Bewegeungsrichtung in der y-Achse des Balls
	 */
	public int getyBallRichtung() {
		
		return this.yBallRichtung;
	}
}