package ch.ffhs.ftoop.pong;

import static org.junit.Assert.*;

import org.junit.Test;

import ch.ffhs.ftoop.pong.Spielfeld;

/**
 * Diese Klasse testet Methoden aus der Klasse Spielfeld
 * 
 * @author Kevin Rossi & Stefan Gerber
 * @since 18/05/2017
 */
public class TestSpielfeld {

	/**
	 * Diese Methode testet die Getter Methode fuer die Punktzahl der Spieler aus der Spielfeld Klasse
	 */
	@Test
	public void testGetPunkteSpielerLinksUndRechts() {

		Spielfeld spielfeld = new Spielfeld();

		assertEquals(spielfeld.getPunkteSpielerLinks(), 0);
		assertEquals(spielfeld.getPunkteSpielerRechts(), 0);
	}

	/**
	 * Diese Methode testet die Methoden um die Punktzahl eines Spielers zu erhoehen
	 */
	@Test
	public void testErhoehePunktestandSpieler() {

		Spielfeld spielfeld = new Spielfeld();

		spielfeld.erhoehePunktestandSpielerLinks(2);
		spielfeld.erhoehePunktestandSpielerRechts(1);

		assertEquals(spielfeld.getPunkteSpielerLinks(), 2);
		assertEquals(spielfeld.getPunkteSpielerRechts(), 1);
	}
}