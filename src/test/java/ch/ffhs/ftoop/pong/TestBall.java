package ch.ffhs.ftoop.pong;

import static org.junit.Assert.*;

import org.junit.Test;

import ch.ffhs.ftoop.pong.Ball;
import ch.ffhs.ftoop.pong.Spielfeld;

/**
 * Diese Klasse testet Methoden aus der Klasse Ball
 * 
 * @author Kevin Rossi & Stefan Gerber
 * @since 18/05/2017
 */
public class TestBall {

	private Spielfeld spielfeld;

	/**
	 * Konstruktor
	 */
	public TestBall() {
		
		this.spielfeld = new Spielfeld();
	}

	/**
	 * Diese Methode testet die Methode ResetBallPosition der Ball Klasse
	 */
	@Test
	public void testResetBallPosition() {
		
		Ball ball = new Ball(this.spielfeld, 40, 40, 12, 12);

		ball.resetPositionBall();

		assertEquals(ball.getxKordinate(), 390);
		assertEquals(ball.getyKordinate(), 275);
		assertEquals(ball.getBallSpeed(), 3);
	}

	/**
	 * Diese Methode testet die Getter Methode fuer die XKoordinate eines Balles
	 */
	@Test
	public void testGetXKordinate() {
		
		Ball ball = new Ball(this.spielfeld, 48, 55, 10, 10);

		assertEquals(ball.getxKordinate(), 55);
	}

	/**
	 * Diese Methode testet die Getter Methode fuer die YKoordinate eines Balles
	 */
	@Test
	public void testGetYKordinate() {
		
		Ball ball = new Ball(this.spielfeld, 48, 55, 10, 10);

		assertEquals(ball.getyKordinate(), 48);
	}

	/**
	 * Diese Methode testet die Getter Methode fuer die Breite und Hoehe eines Balles
	 */
	@Test
	public void testGetBreiteUndGetHoehe() {
		
		Ball ball = new Ball(this.spielfeld, 48, 55, 10, 10);

		assertEquals(ball.getBreiteBall(), 10);
		assertEquals(ball.getHoeheBall(), 10);
	}

	/**
	 * Diese Methode testet die Getter Methode fuer die Geschwindigkeit eines Balles
	 */
	@Test
	public void testGetBallSpeed() {
		
		Ball ball = new Ball(this.spielfeld, 48, 55, 10, 10);

		assertEquals(ball.getBallSpeed(), 3);
	}
}