package ch.ffhs.ftoop.pong;

import static org.junit.Assert.*;

import org.junit.Test;

import ch.ffhs.ftoop.pong.Spieler;
import ch.ffhs.ftoop.pong.Spielfeld;

/**
 * Diese Klasse testet Methoden aus der Klasse Spieler
 * 
 * @author Kevin Rossi & Stefan Gerber
 * @since 18/05/2017
 */
public class TestSpieler {

	private Spielfeld spielfeld;

	/**
	 * Konstruktor
	 */
	public TestSpieler() {

		this.spielfeld = new Spielfeld();
	}

	/**
	 * Diese Methode testet die Getter Methode fuer die XKoordinate eines Spielers
	 */
	@Test
	public void testGetXKordinate() {

		Spieler spieler = new Spieler(this.spielfeld, 48, 55, 10, 10);

		assertEquals(spieler.getxKordinate(), 55);
	}

	/**
	 * Diese Methode testet die Getter Methode fuer die yKoordinate eines Spielers
	 */
	@Test
	public void testGetYKordinate() {

		Spieler spieler = new Spieler(this.spielfeld, 48, 55, 10, 10);

		assertEquals(spieler.getyKordinate(), 48);
	}

	/**
	 * Diese Methode testet die Getter Methode fuer die Breite und Hoehe eines Spielers
	 */
	@Test
	public void testGetBreiteUndGetHoehe() {

		Spieler spieler = new Spieler(this.spielfeld, 48, 55, 10, 80);

		assertEquals(spieler.getBreiteSpieler(), 10);
		assertEquals(spieler.getHoeheSpieler(), 80);
	}

	/**
	 * Diese Methode testet die Getter Methode fuer die Geschwindigkeit eines Spielers
	 */
	@Test
	public void testGetVelY() {

		Spieler spieler = new Spieler(this.spielfeld, 48, 55, 10, 80);

		assertEquals(spieler.getVelY(), 0);
	}

	/**
	 * Diese Methode testet die Setter Methode fuer die Geschwindigkeit eines Spielers
	 */
	@Test
	public void testSetVelY() {

		Spieler spieler = new Spieler(this.spielfeld, 48, 55, 10, 80);

		spieler.setVelY(3);
		assertEquals(spieler.getVelY(), 3);
	}
}